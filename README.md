# hugo-kde
This is the Hugo theme shared among several KDE websites.

## Usage

Add the module to your Hugo website like this:

```shell
hugo mod init https://invent.kde.org/websites/hugo-kde
```

### Updating

Updating is extremely easy, update the `go.mod` with thus command:

```shell
hugo mod get invent.kde.org/websites/hugo-kde
```

You can read more about how Hugo modules work [in the Hugo documentation](https://gohugo.io/hugo-modules/use-modules/).

## Development

You need recent versions of Hugo and Go installed. Then clone

```bash
git clone git@invent.kde.org/websites/hugo-kde
```
Read more in the [wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

## Code style

New code should be compatible with these conventions:
- **Indentation**: 2 spaces
- One line between each CSS declaration
- One SASS module for each logical component.
