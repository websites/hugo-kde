/*
 * SPDX-FileCopyrightText: 2020 (c) Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Video from './video.js';

const video = new Video();
video.convertPeertube();