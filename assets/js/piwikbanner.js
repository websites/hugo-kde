/*
 * SPDX-FileCopyrightText: 2020 (c) Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const setCookie = (cname, cvalue) => {
    let hostParts = window.location.host.split(".")
    let wildcardDomain = hostParts.pop();
    wildcardDomain = `.${hostParts.pop()}.${wildcardDomain}`;

    let d = new Date();
    d.setTime(d.getTime() + (365*24*60*60*1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = `${cname}=${cvalue}; ${expires}; path=/; domain=${wildcardDomain}`;
};

const getCookie = (cname) => {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i=0; i<ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
};

const getOptOutIframe = (language) => {
    let text = '';
    document.querySelectorAll('.teaser').forEach(el => el.classList.add('d-none'));
    if(language === 0) {
        text = `<p>Our website uses piwik, a so called web analyzer service. Piwik uses "cookies", those are text files stored on your computer and enabling us to analyse the usage of the website. For this purpose usage statistics generated from the cookies (including your shortened ip address) are sent to our server and saved for usage analytics, to optimize our websites. During this process your ip address immediately gets anonymized so you as user are completely anonymous to us. The generated information by the cookies are not passed on to a third party. You can prevent the usage of cookies by a corresponding setting in your browser software, but in this case you might not be able to use this website with full functionality.</p>
                <p>If you do not agree with the storage and evaluation of your data from your visit, then you can opt-out of storage and usage via a mouse click. In this case your browser will store a so called Opt-Out-Cookie, which tells Piwik to not analyse any session data. Attention: If you delete all your cookies, your Opt-Out-Cookie will be deleted as well, in which case you probably need to activate it again.</p>`;

        if(navigator.language === 'de') {
            text = `<p>Unsere Website verwendet Piwik, dabei handelt es sich um einen sogenannten Webanalysedienst. Piwik verwendet sog. „Cookies“, das sind Textdateien, die auf Ihrem Computer gespeichert werden und die unsererseits eine Analyse der Benutzung der Webseite ermöglichen. Zu diesem Zweck werden die durch den Cookie erzeugten Nutzungsinformationen (einschließlich Ihrer gekürzten IP-Adresse) an unseren Server übertragen und zu Nutzungsanalysezwecken gespeichert, was der Webseitenoptimierung unsererseits dient. Ihre IP-Adresse wird bei diesem Vorgang umgehend anonymisiert, so dass Sie als Nutzer für uns anonym bleiben. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Webseite werden nicht an Dritte weitergegeben. Sie können die Verwendung der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern, es kann jedoch sein, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können.</p>
                <p>Wenn Sie mit der Speicherung und Auswertung dieser Daten aus Ihrem Besuch nicht einverstanden sind, dann können Sie der Speicherung und Nutzung nachfolgend per Mausklick jederzeit widersprechen. In diesem Fall wird in Ihrem Browser ein sog. Opt-Out-Cookie abgelegt, was zur Folge hat, dass Piwik keinerlei Sitzungsdaten erhebt. Achtung: Wenn Sie Ihre Cookies löschen, so hat dies zur Folge, dass auch das Opt-Out-Cookie gelöscht wird und ggf. von Ihnen erneut aktiviert werden muss.</p>`;
        }

        document.getElementById('main').innerHTML = `${text}
            <iframe width="100%" src="https://stats.kde.org/index.php?module=CoreAdminHome&action=optOut&language=${navigator.language}" /><p><a href="${window.location.protocol}//${window.location.host}">Return to content</a></p>
            <p>We are detecting your language from your browser settings:</p>
            <p><a class="optoutenglish" href="#">See this text in English</a></p>
            <p><a class="optoutgerman" href="#">See this text in German</a></p>`;
    } else if(language === 1) {
        document.getElementById('main').innerHTML = `
            <p>Our website uses piwik, a so called web analyzer service. Piwik uses "cookies", those are text files stored on your computer and enabling us to analyse the usage of the website. For this purpose usage statistics generated from the cookies (including your shortened ip address) are sent to our server and saved for usage analytics, to optimize our websites. During this process your ip address immediately gets anonymized so you as user are completely anonymous to us. The generated information by the cookies are not passed on to a third party. You can prevent the usage of cookies by a corresponding setting in your browser software, but in this case you might not be able to use this website with full functionality.</p>
            <p>If you do not agree with the storage and evaluation of your data from your visit, then you can opt-out of storage and usage via a mouse click. In this case your browser will store a so called Opt-Out-Cookie, which tells Piwik to not analyse any session data. Attention: If you delete all your cookies, your Opt-Out-Cookie will be deleted as well, in which case you probably need to activate it again.</p>
            <iframe width="100%" src="https://stats.kde.org/index.php?module=CoreAdminHome&action=optOut&language=en" /><p><a href="${window.location.protocol}//${window.location.host}">Return to content</a></p>
            <p><a class="optoutgerman" href="#">See this text in German</a></p>`;
    } else if(language === 2) {
        document.getElementById('main').innerHTML = `
            <p>Unsere Website verwendet Piwik, dabei handelt es sich um einen sogenannten Webanalysedienst. Piwik verwendet sog. „Cookies“, das sind Textdateien, die auf Ihrem Computer gespeichert werden und die unsererseits eine Analyse der Benutzung der Webseite ermöglichen. Zu diesem Zweck werden die durch den Cookie erzeugten Nutzungsinformationen (einschließlich Ihrer gekürzten IP-Adresse) an unseren Server übertragen und zu Nutzungsanalysezwecken gespeichert, was der Webseitenoptimierung unsererseits dient. Ihre IP-Adresse wird bei diesem Vorgang umgehend anonymisiert, so dass Sie als Nutzer für uns anonym bleiben. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Webseite werden nicht an Dritte weitergegeben. Sie können die Verwendung der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern, es kann jedoch sein, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können.</p>
                <p>Wenn Sie mit der Speicherung und Auswertung dieser Daten aus Ihrem Besuch nicht einverstanden sind, dann können Sie der Speicherung und Nutzung nachfolgend per Mausklick jederzeit widersprechen. In diesem Fall wird in Ihrem Browser ein sog. Opt-Out-Cookie abgelegt, was zur Folge hat, dass Piwik keinerlei Sitzungsdaten erhebt. Achtung: Wenn Sie Ihre Cookies löschen, so hat dies zur Folge, dass auch das Opt-Out-Cookie gelöscht wird und ggf. von Ihnen erneut aktiviert werden muss.</p>
            <iframe width="100%" src="https://stats.kde.org/index.php?module=CoreAdminHome&action=optOut&language=de" /><p><a href="${window.location.protocol}//${window.location.host}">Zurück zum Inhalt</a></p>
            <p><a class="optoutenglish" href="#">See this text in English</a></p>`;
    }

    document.querySelectorAll('.optoutenglish').forEach(el => {
        el.addEventListener('click', e => {
            e.preventDefault();
            getOptOutIframe(1);
        });
    });

    document.querySelectorAll('.optoutgerman').forEach(el => {
        el.addEventListener('click', e => {
            e.preventDefault();
            getOptOutIframe(2);
        });
    });
};

const addBannerMarkup = () => {
    var wrapper= document.createElement('div');
    wrapper.innerHTML = `
        <div id="cookiebanner" class="cookiebanner">
            <div>KDE uses cookies on our website to help make it better for you. If you would
            like to learn more, or opt out of this feature: <a href="#" class="optoutcookebanner">click here</a></div>
            <button class="btn button" id="dismisscookiebanner">I Understand</button>
        </div>`;

    document.querySelector('body').prepend(wrapper);
    document.getElementById('dismisscookiebanner').addEventListener('click', () => {
        setCookie('cookiebannerdismissed', 'kde');
        document.getElementById('cookiebanner').classList.add('d-none');
    });

    document.querySelectorAll('.optoutcookebanner').forEach(el => {
        el.addEventListener('click', e => {
            e.preventDefault();
            getOptOutIframe(0);
        });
    });
}

if (getCookie('cookiebannerdismissed') !== "kde") {
    addBannerMarkup();
} else {
    console.log('cookie dismissed already');
}