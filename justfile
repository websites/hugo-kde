extract:
  cd {{invocation_directory()}} && PACKAGE=websites-hugo-kde FILENAME=hugo-kde hugoi18n extract pot
fetch:
  cd {{invocation_directory()}} && PACKAGE=websites-hugo-kde FILENAME=hugo-kde hugoi18n fetch po -b l10n-kf5
compile:
  cd {{invocation_directory()}} && PACKAGE=websites-hugo-kde FILENAME=hugo-kde hugoi18n compile po
generate:
  cd {{invocation_directory()}} && PACKAGE=websites-hugo-kde hugoi18n generate -k
